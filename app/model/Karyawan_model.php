<?php

class Karyawan_model{
  private $table = 'karyawan';
  private $db;

  public function __construct(){
    $this->db = new Database;
  }
public function getAllKaryawan(){
    $this->db->query('SELECT * FROM '.$this->table);
    return $this->db->resultSet();
}


public function getKaryawanById($id){
  $this->db->query('SELECT * FROM '.$this->table.' WHERE id=:id');
  $this->db->bind('id',$id);
  return $this->db->single();
}
public function tambahDataKaryawan($data){
    $query = "INSERT INTO karyawan (ID, nama, Posisi) VALUES (:ID, :nama, :posisi)";
    $this->db->query($query);
    $this->db->bind('ID', $data['ID']);
    $this->db->bind('nama', $data['nama']);
    $this->db->bind('posisi', $data['posisi']);

    $this->db->execute();

    return $this->db->rowCount();
}
public function hapusDataKaryawan($id){
    $query = "DELETE FROM karyawan WHERE ID = :ID";
    $this->db->query($query);
    $this->db->bind('ID',$id);
    $this->db->execute();

    return $this->db->rowCount();
}

public function editDataKaryawan($data) {
  $query = "UPDATE karyawan SET
          ID = :ID,
          nama = :nama,
          Posisi = :posisi
          WHERE ID = :ID";
        
  $this->db->query($query);
  $this->db->bind('ID', $data['ID']);
  $this->db->bind('nama', $data['nama']);
  $this->db->bind('posisi', $data['posisi']); // Menggunakan 'posisi' sesuai dengan placeholder dalam kueri

  $this->db->execute();

  return $this->db->rowCount();
}


}