<?php

class About extends Controller{
    
    public function index($nama='King',$pekerjaan='Cyber Security',$umur=19){
        $data['nama'] = $nama;
        $data['pekerjaan'] = $pekerjaan;
        $data['umur'] = $umur;

        $data['judul'] = 'About ME';
        $this->view('templetes/header',$data);
        $this->view('about/index',$data);
        $this->view('templetes/footer');
    }
    public function page(){
        $data['judul'] = 'Page Data';
    $this->view('templetes/header',$data);
    $this->view('about/page');
    $this->view('templetes/footer');
}}