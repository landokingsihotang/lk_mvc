<?php

class Karyawan extends Controller{
    public function index(){
        $data['judul'] = "Daftar Karyawan";
        $data['karyawan'] = $this->model('Karyawan_model')->getAllKaryawan();
        $this->view('templetes/header',$data);
        $this->view('karyawan/index',$data);
        $this->view('templetes/footer');
    }

    public function add(){
        if ($this->model('Karyawan_model')->tambahDataKaryawan($_POST)>0){
            header('Location: http://localhost/MVC_WEB_UNPAS/public/karyawan');
            exit;
        }
    }
    public function delete($id){
        if ($this->model('Karyawan_model')->hapusDataKaryawan($id)>0){
            header('Location: http://localhost/MVC_WEB_UNPAS/public/karyawan');
            exit;
        }
    }
    // public function edit($id){
    //     if ($this->model('Karyawan_model')->hapusDataKaryawan($id)>0){
    //         header('Location: http://localhost/MVC_WEB_UNPAS/public/karyawan');
    //         exit;
    //     }
    // }

    public function getEdit(){
     echo json_encode(
        $this->model('Karyawan_model')->getKaryawanById($_POST['id'])
    );
    }

    public function edit()
    {
        if ($this->model('Karyawan_model')->editDataKaryawan($_POST)>0){
            header('Location: http://localhost/MVC_WEB_UNPAS/public/karyawan');
            exit;
        }
    }
    
 
    
}