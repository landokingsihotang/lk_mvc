<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman View</title>
</head>
<body> -->
<div class="container">
<div class="jumbotron">
        <h1>Solusi Bisnis Inovatif untuk Masa Depan</h1>
        <p>Kami menyediakan layanan konsultasi dan pengembangan teknologi terbaik untuk memenuhi kebutuhan bisnis Anda.</p>
        <a href="#">Pelajari Lebih Lanjut</a>
    </div>
    
        <h2>Tentang Kami</h2>
        <p>Kami adalah perusahaan yang berkomitmen untuk memberikan layanan terbaik dalam industri ini. Dengan pengalaman lebih dari 10 tahun, kami telah melayani banyak klien dan membangun reputasi yang solid.</p>
        
        <h2>Layanan Kami</h2>
        <ul>
            <li>Jasa Konsultasi Bisnis</li>
            <li>Pengembangan Aplikasi Web dan Mobile</li>
            <li>Pengelolaan Proyek</li>
            <li>Analisis Data</li>
        </ul>

        <h2>Kontak Kami</h2>
        <p>Jika Anda tertarik atau memiliki pertanyaan, silakan hubungi kami:</p>
        <p>Email: info@perusahaanxyz.com</p>
        <p>Telepon: (123) 456-7890</p>
    </div>
<!-- </body>
</html> -->