<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?=$data['judul'];?> </title>
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="localhost\MVC_WEB_UNPAS\public\css\form.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
$(function() {
  // Saat tombol "Edit" ditekan
  $('.tampilEdit a').on('click', function() {
    // Ubah teks tombol menjadi "Edit" dan atribut data-bs-target modal
    $('#formModal button[type="submit"]').text('Edit Data');
    $('#formModal button[type="button"]').removeClass('btn-primary').addClass('btn-secondary');
    $('#formModal form').attr('action', 'http://localhost/MVC_WEB_UNPAS/public/karyawan/edit');
    const id = $(this).data('id');

    $.ajax({
      url: 'http://localhost/MVC_WEB_UNPAS/public/karyawan/getEdit',
      data: { id: id },
      method: 'post',
      dataType: 'json',
      success: function(data) {
        // Mengisi nilai input dalam modal dengan data yang diterima dari server
        $('#idKaria').val(data.ID);
        $('#namaKar').val(data.nama);
        $('#posisiKar').val(data.Posisi);
      }
    });
  });

  // Reset teks tombol saat modal ditutup
  $('#formModal').on('hidden.bs.modal', function() {
    $('#formModal button[type="submit"]').text('Tambah Data');
    $('#formModal button[type="button"]').removeClass('btn-secondary').addClass('btn-primary');
    // Mengosongkan nilai input dalam modal saat modal ditutup
    $('#ID').val('');
    $('#nama').val('');
    $('#posisi').val('');
  });
});




</script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <!-- <a class="navbar-brand" href="http://localhost/MVC_WEB_UNPAS/public">Navbar</a> -->
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="http://localhost/MVC_WEB_UNPAS/public">Home</a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link active" href="http://localhost/MVC_WEB_UNPAS/public/about">About</a>
        </li> -->
        <li class="nav-item">
          <a class="nav-link active" href="http://localhost/MVC_WEB_UNPAS/public/karyawan">Karyawan</a>
        </li>
      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>
    