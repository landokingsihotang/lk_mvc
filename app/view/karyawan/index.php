<div class="container">
        <h2 class="mt-5 mb-3">Daftar Karyawan</h2>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#formModal">
  Tambah Data
</button>
        <table class="table">
            <thead>
                <tr>
                    <th>ID Karyawan</th>
                    <th>Nama Karyawan</th>
                    <th>Posisi</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // Pastikan variabel $proker berisi hasil query
                if (!empty($data['karyawan'])) {
                    foreach ($data['karyawan'] as $row) {
                ?>
                        <tr>
                            <td class="align-middle"><?php echo $row['ID'] ?></td>
                            <td class="align-middle"><?php echo $row['nama'] ?></td>
                            <td class="align-middle"><?php echo $row['Posisi'] ?></td>
                            <td class="tampilEdit"><a href="http://localhost/MVC_WEB_UNPAS/public/karyawan/edit/<?php echo $row['ID'] ?>" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#formModal" data-id="<?=$row['ID'];?>">Edit</a></td>
                            <td class="align-middle" ><a  href="http://localhost/MVC_WEB_UNPAS/public/karyawan/delete/<?php echo $row['ID'] ?>" class="btn btn-danger" onclick="return confirm('Yakin Untuk Menghapus Data?')">Delete</a></td>
                            
                        </tr>
                <?php
                    }
                } else {
                    echo '<tr><td colspan="5">Tidak ada data Karyawan.</td></tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="JudulModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="JudulModal">Tambah Data</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form action="http://localhost/MVC_WEB_UNPAS/public/karyawan/add" method="post">
        <div class="mb-3">
    <label for="id" class="form-label">ID Karyawan</label>
    <input type="number" class="form-control" type="number" id="idKaria" name="ID" required />
  </div>
        <div class="mb-3">
    <label for="nama" class="form-label">Nama</label>
    <input type="text" class="form-control" type="text" id="namaKar" name="nama" required />
  </div>
        <div class="mb-3">
    <label for="posisi" class="form-label">Posisi</label>
    <input type="text" class="form-control" type="text" id="posisiKar" name="posisi" required />
  </div>
        <br>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
      </div>
    </form>
      </div>
    </div>
  </div>
</div>